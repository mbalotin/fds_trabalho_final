/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UInterface;

import Business.Usuario;
import Business.UsuarioService;
import Business.UsuarioServiceImpl;
import Business.exceptions.InvalidUsuarioException;
import Business.exceptions.NoUserFoundException;
import Business.exceptions.UserAlreadyExistsException;
import java.sql.SQLException;

public class UsuarioFacade {

    protected UsuarioService userServ;

    public UsuarioFacade() throws ClassNotFoundException {
        userServ = new UsuarioServiceImpl();
    }

    /**
     * Busca usuario pelo numero de documento
     *
     * @param doc documento
     * @return Objeto do Usuario
     * @throws Business.exceptions.NoUserFoundException CAso não exista usuario
     * com o odcumento informado
     */
    public Usuario buscarUsuario(String doc) throws NoUserFoundException {
        return userServ.buscaUsuarioByDoc(doc);
    }

    /**
     * Cria um novo usuario
     *
     * @param nome Nome do usuario
     * @param doc documento do usuario
     * @param email email do usuario
     * @param categoria
     * @return Objeto cadastrado do usuario
     * @throws Business.exceptions.UserAlreadyExistsException caso documento ja
     * exista
     * @throws Business.exceptions.InvalidUsuarioException CASo a categoria não seja "L"(Leiloeiro) ou "C"(Comum).
     */
    public Usuario criarUsuario(String nome, String doc, String email, String categoria) throws UserAlreadyExistsException, InvalidUsuarioException, SQLException {
        if (!"L".equalsIgnoreCase(String.valueOf(categoria)) && !"C".equalsIgnoreCase(String.valueOf(categoria))) {
            throw new InvalidUsuarioException();
        }
        Usuario novoUsuario = new Usuario();
        novoUsuario.setDocumento(doc);
        novoUsuario.setEmail(email);
        novoUsuario.setNome(nome);
        novoUsuario.setCategoria(categoria);
        return userServ.cadastrar(novoUsuario);
    }

}
