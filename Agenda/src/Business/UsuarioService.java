/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.NoUserFoundException;
import Business.exceptions.UserAlreadyExistsException;
import java.sql.SQLException;

public interface UsuarioService {
    
    Usuario buscaUsuarioByDoc(String documento) throws NoUserFoundException;
 // Usuario buscaUsuarioById(Integer id) throws NoUserFoundException;           acho que pode ter a busca só pelo doc
    Usuario cadastrar(Usuario novoUsuario) throws UserAlreadyExistsException, SQLException;
    
}
