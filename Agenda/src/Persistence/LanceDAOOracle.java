/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LanceDAOOracle implements LanceDAO {


    @Override
    public LanceTO darLance(LanceTO lance) {

        try {
            String user = "";
            String password = "";
            Class.forName("oracle.jdbc.driver.OracleDriver");

            Connection con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", user, password);
            String sql = "insert into LANCE"
                    + "(Lote, Lance_id, CPF_CNPJ, Valor) "
                    + "values (?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, lance.getLote());
            stmt.setInt(2, lance.getLanceId());
            stmt.setString(3, lance.getCpf_cnpj());
            stmt.setDouble(4, lance.getValor());
            stmt.execute();
            stmt.close();
            return lance;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LanceDAOOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void removerLance(Integer lanceId) {
       
        try {
            String user = "";
            String password = "";
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", user, password);
            String sql = "DELETE from LANCE where Lance_id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, lanceId);
            ResultSet rs = stmt.executeQuery();
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LanceDAOOracle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<LanceTO> buscarLancesByLeilao(Integer lote) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
