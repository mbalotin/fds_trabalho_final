/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.InvalidLanceException;
import Business.exceptions.LeilaoEncerradoException;
import Business.exceptions.NoLeilaoFoundException;
import Business.exceptions.NoUserFoundException;
import Business.exceptions.PersistenceException;
import Persistence.LeilaoDAO;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.anyObject;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author mbalotin
 */
public class LeilaoServiceImplTest {

    @Mock
    LeilaoDAO dao;

    @InjectMocks
    LeilaoServiceImpl leilaoServ;

    Leilao LEILAO_TEST;
    Usuario USUARIO_TEST;
    int LOTE_TEST;

    public LeilaoServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        USUARIO_TEST = new Usuario();

        LOTE_TEST = 0;

        LEILAO_TEST = new Leilao();
        LEILAO_TEST.setCPF_CNPJ("000000");
        LEILAO_TEST.setLote(LOTE_TEST);
        LEILAO_TEST.setPrecoMinimo(0.0);
        LEILAO_TEST.setStatus("A");
        LEILAO_TEST.setLances(new ArrayList<>());
        LEILAO_TEST.setCriador(USUARIO_TEST);
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testGerarLeilao() throws InvalidLanceException, NoUserFoundException {
        leilaoServ.gerarLeilao(LEILAO_TEST);
        verify(dao, times(1)).criar(anyObject());
    }

    @Test
    public void testEncerrarLeilao() throws LeilaoEncerradoException, PersistenceException, Exception {
        leilaoServ.encerrarLeilao(LEILAO_TEST);
        verify(dao, times(1)).encerrar(anyObject());

    }

    @Test
    public void testEncerrarLeilaoLote() throws LeilaoEncerradoException, PersistenceException {
        leilaoServ.encerraLeilaoPorLote(LOTE_TEST);
        verify(dao, times(1)).remover(LOTE_TEST);
    }

    @Test
    public void buscarLeilaoLote() throws NoLeilaoFoundException {
        leilaoServ.buscarLeilaoByLote(LOTE_TEST);
        verify(dao, times(1)).buscaByLote(LOTE_TEST);
    }

    @Test
    public void testEncerrarAbertos() {
        assertTrue(true);
    }

    @Test
    public void testEncerrarEncerrados() {
        assertTrue(true);
    }
}
