/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.UserAlreadyExistsException;
import Persistence.UsuarioDAO;
import Persistence.UsuarioTO;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author mbalotin
 */
public class UsuarioServiceImplTest {

    @Mock
    UsuarioDAO dao;

    @InjectMocks
    UsuarioServiceImpl userService;
    
    Usuario USUARIO_TEST;

    public UsuarioServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        
        USUARIO_TEST = new Usuario();
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCadastrar() throws UserAlreadyExistsException, SQLException {
        UsuarioTO value = new UsuarioTO();
        value.setId(1);
        when(dao.cadastrar(any())).thenReturn(value);
        userService.cadastrar(USUARIO_TEST);
        assertEquals(value.getId(), USUARIO_TEST.getId());
    }

    @Test
    public void testBuscar() {

    }

}
